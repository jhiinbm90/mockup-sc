//
//  ViewController.h
//  RotateWheelProject
//
//  Created by 임종화 on 2015. 6. 12..
//  Copyright (c) 2015년 임종화. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RotateWheel.h"
#import "TabBarViewController.h"
#import "SWRevealViewController.h"

@interface ViewController : UIViewController <RotateProtocol>
{
    NSString *url;
}

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (weak, nonatomic) IBOutlet UIButton *sectorButton;

@end


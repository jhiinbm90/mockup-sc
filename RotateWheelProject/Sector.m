
//
//  Selector.m
//  RotateWheelProject
//
//  Created by 임종화 on 2015. 6. 12..
//  Copyright (c) 2015년 임종화. All rights reserved.
//

#import "Sector.h"

@implementation Sector

@synthesize minValue, maxValue, midValue, sector;

- (NSString *) description {
    return [NSString stringWithFormat:@"%i | %f, %f, %f", self.sector, self.minValue, self.midValue, self.maxValue];
}

@end

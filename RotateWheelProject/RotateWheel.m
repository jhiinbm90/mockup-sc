//
//  RotateWheel.m
//  RotateWheelProject
//
//  Created by 임종화 on 2015. 6. 12..
//  Copyright (c) 2015년 임종화. All rights reserved.
//

#import "RotateWheel.h"

@interface RotateWheel()
- (void) drawWheel;
- (float) calculateDistanceFromCenter:(CGPoint)point;
- (void) buildSectorsEven;
- (void) buildSectorsOdd;
@end

static float deltangle;

@implementation RotateWheel

@synthesize delegate, container, numberOfSections, startTransform, sectors, currentSector;

- (id) initWithFrame:(CGRect)frame andDelegate:(id)del withSections:(int)sectionsNumber {
    if((self = [super initWithFrame:frame])){
        self.numberOfSections = sectionsNumber;
        self.delegate = del;
        [self drawWheel];
    }
    return  self;
}

- (void) rotate {
    CGAffineTransform t = CGAffineTransformRotate(container.transform, -0.78);
    container.transform = t;
}

- (BOOL) beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {

    CGPoint touchPoint = [touch locationInView:self];
    
    float dist = [self calculateDistanceFromCenter:touchPoint];
    // 1.2 - Filter out touches too close to the center
    if (dist < 70 || dist > 140)
    {
        // forcing a tap to be on the ferrule
        NSLog(@"ignoring tap (%f,%f)", touchPoint.x, touchPoint.y);
        return NO;
    }
    
    float dx = touchPoint.x - container.center.x;
    float dy = touchPoint.y - container.center.y;
    
    deltangle = atan2(dy, dx);
    
    startTransform = container.transform;
    return YES;
}

- (BOOL) continueTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    CGPoint pt = [touch locationInView:self];
    float dx = pt.x - container.center.x;
    float dy = pt.y - container.center.y;
    float ang = atan2(dy, dx);
    float angleDifference = deltangle - ang;
    container.transform = CGAffineTransformRotate(startTransform, -angleDifference);
    
    return YES;
}

- (void) endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    CGFloat radians = atan2f(container.transform.b, container.transform.a);
    CGFloat newVal = 0.0;
    
    self.currentSector = 0;
    
    for(Sector *s in sectors){
        if(radians > s.minValue && radians < s.maxValue) {
            newVal = radians - s.midValue;
            currentSector = s.sector;
            break;
        }
    }
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.2];
    CGAffineTransform t = CGAffineTransformRotate(container.transform, -newVal);
    container.transform = t;
    [UIView commitAnimations];
    
    [self.delegate wheelDidChangeValue:self.currentSector];
}

- (float) calculateDistanceFromCenter:(CGPoint)point {
    CGPoint center = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2);
    float dx = point.x - center.x;
    float dy = point.y - center.y;
    return sqrt(dx*dx + dy*dy);
}

- (void) drawWheel {
    container = [[UIView alloc] initWithFrame:self.frame];
    CGFloat angleSize = 2*M_PI/numberOfSections;
    
    for(int i = 0; i < numberOfSections; i++){
        NSString *btnTitle = @"";
        switch (i) {
            case 0:
                btnTitle = @"Naver";
                break;
                
            case 1:
                btnTitle = @"Nate";
                break;
            
            case 2:
                btnTitle = @"Daum";
                break;
                
            case 3:
                btnTitle = @"Google";
                break;
                
            case 4:
                btnTitle = @"YouTube";
                break;
                
            case 5:
                btnTitle = @"Bing";
                break;
                
            case 6:
                btnTitle = @"Apple";
                break;
                
            case 7:
                btnTitle = @"Android";
                break;
                
            case 8:
                btnTitle = @"Yahoo";
                break;
                
            default:
                break;
        }
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 140, 40)];
        label.text = btnTitle;
        label.layer.anchorPoint = CGPointMake(1.0f, 0.5f);
        label.layer.position = CGPointMake(container.bounds.size.width/2.0, container.bounds.size.height/2.0);
        label.transform = CGAffineTransformMakeRotation(angleSize * i);
        [container addSubview:label];
        
        sectors = [NSMutableArray arrayWithCapacity:numberOfSections];
        if(numberOfSections %2 == 0){
            [self buildSectorsEven];
        } else {
            [self buildSectorsOdd];
        }
    }
    
    container.userInteractionEnabled = NO;
    [self addSubview:container];
    

    [self.delegate wheelDidChangeValue:self.currentSector];
}

- (void) buildSectorsOdd {
    CGFloat fanWidth = M_PI*2/numberOfSections;
    CGFloat mid = 0;
    
    for (int i=0; i < numberOfSections; i++) {
        Sector *sector = [[Sector alloc] init];
        sector.midValue = mid;
        sector.minValue = mid - (fanWidth/2);
        sector.maxValue = mid + (fanWidth/2);
        sector.sector = i;
        mid -= fanWidth;
        if(sector.minValue < - M_PI){
            mid = -mid;
            mid -= fanWidth;
        }
        [sectors addObject:sector];
    }
}

- (void) buildSectorsEven {
    CGFloat fanWidth = M_PI*2/numberOfSections;
    CGFloat mid = 0;

    for (int i = 0; i < numberOfSections; i++) {
        Sector *sector = [[Sector alloc] init];
        sector.midValue = mid;
        sector.minValue = mid - (fanWidth/2);
        sector.maxValue = mid + (fanWidth/2);
        sector.sector = i;
        if (sector.maxValue-fanWidth < - M_PI) {
            mid = M_PI;
            sector.midValue = mid;
            sector.minValue = fabsf(sector.maxValue);
        }
        mid -= fanWidth;
        [sectors addObject:sector];
    }
}

- (void) setCenterPosition:(CGPoint)position {
    self.center = position;
}

@end

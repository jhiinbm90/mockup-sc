#import <Foundation/Foundation.h>

@protocol RotateProtocol <NSObject>

- (void) wheelDidChangeValue:(int)newValue;

@end
//
//  TabBarViewController.m
//  SampleSCBank
//
//  Created by 임종화 on 2015. 6. 10..
//  Copyright (c) 2015년 임종화. All rights reserved.
//

#import "TabBarViewController.h"

@implementation TabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"Url : %@",_urlString);
    [[self viewControllers] objectAtIndex:0];
    [[self viewControllers] objectAtIndex:1];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

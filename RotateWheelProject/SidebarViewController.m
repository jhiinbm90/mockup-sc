//
//  SidebarViewController.m
//  SlideMenuSample
//
//  Created by 임종화 on 2015. 6. 23..
//  Copyright (c) 2015년 임종화. All rights reserved.
//

#import "SidebarViewController.h"
#import "SWRevealViewController.h"
#import "WebViewController.h"

@implementation SidebarViewController {
    NSArray *menuItems;
}

- (void) viewDidLoad {
    [super viewDidLoad];
    
    menuItems = @[@"title",@"naver",@"nate",@"daum",@"google",@"youtube",@"bing",@"apple",@"android",@"yahoo"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return menuItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellTitle = [menuItems objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellTitle forIndexPath:indexPath];
    
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    UINavigationController *destViewController = (UINavigationController*)segue.destinationViewController;
    destViewController.title = [[menuItems objectAtIndex:indexPath.row] capitalizedString];
    
    if([segue.identifier isEqualToString:@"showWebView"]) {
        UINavigationController *navController = segue.destinationViewController;
        WebViewController *webViewController = [navController childViewControllers].firstObject;
        NSString *url = [NSString stringWithFormat:@"http://www.%@.com",[menuItems objectAtIndex:indexPath.row]];
        webViewController.url = url;
    }
}
@end
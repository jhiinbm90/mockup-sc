//
//  WebViewController.h
//  SlideMenuSample
//
//  Created by 임종화 on 2015. 6. 23..
//  Copyright (c) 2015년 임종화. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (strong, nonatomic) NSString *url;
@end

//
//  ViewController.m
//  RotateWheelProject
//
//  Created by 임종화 on 2015. 6. 12..
//  Copyright (c) 2015년 임종화. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    SWRevealViewController *revealViewController = [[SWRevealViewController alloc] init];
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    RotateWheel *wheel = [[RotateWheel alloc] initWithFrame:CGRectMake(0, 0, 300, 300) andDelegate:self withSections:9];
    [wheel setCenterPosition:CGPointMake(self.view.center.x, self.view.center.y)];
    [self.view addSubview:wheel];
    
    url = @"naver.com";
    [_sectorButton addTarget:self action:@selector(webViewCall) forControlEvents:UIControlEventTouchUpInside];
    [_sectorButton setTitle:@"Naver" forState:UIControlStateNormal];
    [_sectorButton setFrame:CGRectMake(self.view.center.x-70, self.view.center.y-20, 140, 40)];
    [self.view addSubview:_sectorButton];
    
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"showTabBar"]){
        TabBarViewController *tbvc = [segue destinationViewController];
        tbvc.urlString = url;
    }
}

- (void) wheelDidChangeValue:(int)newValue {
    NSString *title = @"";
    switch (newValue) {
        case 0:
            title = @"Naver";
            url = @"naver.com";
            break;
            
        case 1:
            title = @"Nate";
            url = @"nate.com";
            break;
            
        case 2:
            title = @"Daum";
            url = @"daum.net";
            break;
            
        case 3:
            title = @"Google";
            url = @"google.co.kr";
            break;
            
        case 4:
            title = @"YouTube";
            url = @"youtube.com";
            break;
            
        case 5:
            title = @"Bing";
            url = @"bing.com";
            break;
            
        case 6:
            title = @"Apple";
            url = @"apple.com";
            break;
            
        case 7:
            title = @"Android";
            url = @"android.com";
            break;
            
        case 8:
            title = @"Yahoo";
            url = @"yahoo.com";
            break;
            
        default:
            break;
    }
    
    
    [self.sectorButton setTitle:title forState:UIControlStateNormal];
}

- (void) webViewCall {
    NSLog(@"Clicked!");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

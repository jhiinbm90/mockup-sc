//
//  TabBarViewController.h
//  SampleSCBank
//
//  Created by 임종화 on 2015. 6. 10..
//  Copyright (c) 2015년 임종화. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FirstViewController.h"
#import "SecondViewController.h"

@interface TabBarViewController : UITabBarController
@property (strong, nonatomic) NSString *urlString;
@end

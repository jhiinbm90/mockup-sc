//
//  FirstViewController.m
//  SampleSCBank
//
//  Created by 임종화 on 6/5/15.
//  Copyright (c) 2015 임종화. All rights reserved.
//

#import "FirstViewController.h"

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    TabBarViewController *tabBar = (TabBarViewController*) self.tabBarController;
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.%@",tabBar.urlString]]];
    NSLog([NSString stringWithFormat:@"http://www.%@",tabBar.urlString]);
    [_webView loadRequest:urlRequest];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

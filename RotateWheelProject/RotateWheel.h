//
//  RotateWheel.h
//  RotateWheelProject
//
//  Created by 임종화 on 2015. 6. 12..
//  Copyright (c) 2015년 임종화. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoateProtocol.h"
#import "Sector.h"

@interface RotateWheel : UIControl

@property (weak) id <RotateProtocol> delegate;
@property (nonatomic, strong) UIView *container;
@property int numberOfSections;
@property CGAffineTransform startTransform;
@property (nonatomic, strong) NSMutableArray *sectors;
@property int currentSector;

- (id) initWithFrame:(CGRect)frame andDelegate:(id)del withSections:(int)sectionsNumber;
- (void) rotate;
- (void) setCenterPosition:(CGPoint)position;
@end

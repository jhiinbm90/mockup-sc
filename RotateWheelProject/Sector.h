//
//  Selector.h
//  RotateWheelProject
//
//  Created by 임종화 on 2015. 6. 12..
//  Copyright (c) 2015년 임종화. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Sector : NSObject

@property float minValue;
@property float maxValue;
@property float midValue;
@property int sector;

@end

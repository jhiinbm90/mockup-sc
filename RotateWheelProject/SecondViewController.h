//
//  SecondViewController.h
//  SampleSCBank
//
//  Created by 임종화 on 6/5/15.
//  Copyright (c) 2015 임종화. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TabBarViewController.h"

@interface SecondViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@end

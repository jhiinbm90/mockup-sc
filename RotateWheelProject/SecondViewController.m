//
//  SecondViewController.m
//  SampleSCBank
//
//  Created by 임종화 on 6/5/15.
//  Copyright (c) 2015 임종화. All rights reserved.
//

#import "SecondViewController.h"

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    TabBarViewController *tabBar = (TabBarViewController*) self.tabBarController;
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://mail.%@",tabBar.urlString]]];
    NSLog([NSString stringWithFormat:@"http://mail.%@",tabBar.urlString]);
    [_webView loadRequest:urlRequest];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
